package com.example.springbootcrud.configuration;

import com.example.springbootcrud.product.adapter.out.persistence.ProductMapper;
import com.example.springbootcrud.product.adapter.out.persistence.ProductPersistenceAdapter;
import com.example.springbootcrud.product.adapter.out.persistence.ProductRepository;
import com.example.springbootcrud.product.application.port.in.ProductUseCase;
import com.example.springbootcrud.product.application.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProductConfiguration {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    private ProductPersistenceAdapter productPersistenceAdapter(
            ProductRepository productRepository,
            ProductMapper productMapper) {
        return new ProductPersistenceAdapter(productRepository, productMapper);
    }

    @Bean
    public ProductUseCase productUseCase() {
        return new ProductService(productPersistenceAdapter(productRepository, productMapper));
    }

}
