package com.example.springbootcrud.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Value;

import java.util.List;

@AllArgsConstructor
@Getter
public class ErrorResponse {

    private Boolean success;
    private String message;
    private List<Error> errors;

    @Value
    public static class Error {
        private String field;
        private String message;
    }

}
