package com.example.springbootcrud.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SuccessResponse {

    private Boolean success;
    private String message;
    private Object data;

    public SuccessResponse(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }

}
