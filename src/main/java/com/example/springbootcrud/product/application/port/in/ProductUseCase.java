package com.example.springbootcrud.product.application.port.in;

import com.example.springbootcrud.product.domain.Product;

import java.util.List;

public interface ProductUseCase {

    List<Product> getAllProducts();
    Product addProduct(ProductRequestPayload requestPayload);
    Product updateProductDetails(Long id, ProductRequestPayload requestPayload);
    void deleteProduct(Long id);

}
