package com.example.springbootcrud.product.application.port.out;

import com.example.springbootcrud.product.domain.Product;

import java.util.List;

public interface ProductPort {

    Boolean checkExistingProductById(Long id);

    List<Product> getAllProductsFromPersistence();
    Product addProductToPersistence(Product product);
    Product updateProductInPersistence(Product product);
    void deleteProductFromPersistence(Long id);

}
