package com.example.springbootcrud.product.application.service;

import com.example.springbootcrud.exception.ResourceNotFoundException;
import com.example.springbootcrud.product.application.port.in.ProductRequestPayload;
import com.example.springbootcrud.product.application.port.in.ProductUseCase;
import com.example.springbootcrud.product.application.port.out.ProductPort;
import com.example.springbootcrud.product.domain.Product;

import java.util.List;

public class ProductService implements ProductUseCase {

    private final ProductPort productPort;

    public ProductService(ProductPort productPort) {
        this.productPort = productPort;
    }

    @Override
    public List<Product> getAllProducts() {
        return productPort.getAllProductsFromPersistence();
    }

    @Override
    public Product addProduct(ProductRequestPayload requestPayload) {
        Product newProduct = new Product();
        newProduct.setName(requestPayload.getName());
        newProduct.setDescription(requestPayload.getDescription());
        newProduct.setPrice(requestPayload.getPrice());
        return productPort.addProductToPersistence(newProduct);
    }

    @Override
    public Product updateProductDetails(Long id, ProductRequestPayload requestPayload) {

        Boolean isProductExist = productPort.checkExistingProductById(id);
        if (!isProductExist) {
            throw new ResourceNotFoundException("Product not found");
        }

        Product updatedProduct = new Product();
        updatedProduct.setId(id);
        updatedProduct.setName(requestPayload.getName());
        updatedProduct.setDescription(requestPayload.getDescription());
        updatedProduct.setPrice(requestPayload.getPrice());
        return productPort.updateProductInPersistence(updatedProduct);
    }

    @Override
    public void deleteProduct(Long id) {

        Boolean isProductExist = productPort.checkExistingProductById(id);
        if (!isProductExist) {
            throw new ResourceNotFoundException("Product not found");
        }

        productPort.deleteProductFromPersistence(id);
    }

}
