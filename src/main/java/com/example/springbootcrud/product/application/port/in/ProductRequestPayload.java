package com.example.springbootcrud.product.application.port.in;

import com.example.springbootcrud.utility.SelfValidation;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ProductRequestPayload extends SelfValidation<ProductRequestPayload> {

    @NotEmpty(message = "Name cannot be empty")
    private String name;
    @NotEmpty(message = "Description cannot be empty")
    private String description;
    @NotNull(message = "Price cannot be empty")
    private Float price;

    public ProductRequestPayload(String name, String description, Float price) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.validateSelf();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Float getPrice() {
        return price;
    }

}
