package com.example.springbootcrud.product.adapter.in.web;

import com.example.springbootcrud.product.application.port.in.ProductRequestPayload;
import com.example.springbootcrud.product.application.port.in.ProductUseCase;
import com.example.springbootcrud.product.domain.Product;
import com.example.springbootcrud.response.SuccessResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    private final ProductUseCase productUseCase;

    public ProductController(ProductUseCase productUseCase) {
        this.productUseCase = productUseCase;
    }

    @GetMapping("/")
    public String index() {
        return "Source code: https://gitlab.com/kristiansandratama/spring-boot-crud";
    }

    @GetMapping("api/v1/products")
    public ResponseEntity<Object> getAllProducts() {
        List<Product> data = productUseCase.getAllProducts();
        SuccessResponse successResponse = new SuccessResponse(true, "Products found", data);
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    @PostMapping("api/v1/products")
    public ResponseEntity<Object> addProduct(@RequestBody ProductRequestPayload requestBody) {
        Product data = productUseCase.addProduct(requestBody);
        SuccessResponse successResponse = new SuccessResponse(true, "Product added successfully", data);
        return new ResponseEntity<>(successResponse, HttpStatus.CREATED);
    }

    @PutMapping("api/v1/products/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable Long id,
                                                @RequestBody ProductRequestPayload requestBody) {
        Product data = productUseCase.updateProductDetails(id, requestBody);
        SuccessResponse successResponse = new SuccessResponse(true, "Product updated successfully", data);
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    @DeleteMapping("api/v1/products/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable Long id) {
        productUseCase.deleteProduct(id);
        SuccessResponse successResponse = new SuccessResponse(true, "Product deleted successfully");
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

}
