package com.example.springbootcrud.product.adapter.out.persistence;

import com.example.springbootcrud.product.domain.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

    Product mapToDomainEntity(ProductJpaEntity productJpaEntity) {
        return new Product(
                productJpaEntity.getId(),
                productJpaEntity.getName(),
                productJpaEntity.getDescription(),
                productJpaEntity.getPrice());
    }

    ProductJpaEntity mapToJpaEntity(Product product) {
        return new ProductJpaEntity(
                product.getId(),
                product.getName(),
                product.getDescription(),
                product.getPrice());
    }

}
