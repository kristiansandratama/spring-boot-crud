package com.example.springbootcrud.product.adapter.out.persistence;

import com.example.springbootcrud.product.application.port.out.ProductPort;
import com.example.springbootcrud.product.domain.Product;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductPersistenceAdapter implements ProductPort {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    @Override
    public Boolean checkExistingProductById(Long id) {
        Optional<ProductJpaEntity> product = productRepository.findById(id);
        if (!product.isPresent()) {
            return false;
        }
        return true;
    }

    @Override
    public List<Product> getAllProductsFromPersistence() {
        List<ProductJpaEntity> productJpaEntityList = productRepository.findAll();
        List<Product> products = new ArrayList<>();
        for (ProductJpaEntity productJpaEntity : productJpaEntityList) {
            products.add(productMapper.mapToDomainEntity(productJpaEntity));
        }
        return products;
    }

    @Override
    public Product addProductToPersistence(Product product) {
        ProductJpaEntity productJpaEntity = productRepository.save(productMapper.mapToJpaEntity(product));
        return productMapper.mapToDomainEntity(productJpaEntity);
    }

    @Override
    public Product updateProductInPersistence(Product product) {
        ProductJpaEntity productJpaEntity = productRepository.save(productMapper.mapToJpaEntity(product));
        return productMapper.mapToDomainEntity(productJpaEntity);
    }

    @Override
    public void deleteProductFromPersistence(Long id) {
        productRepository.deleteById(id);
    }

}
